package br.com.scm.sistemacontrolemateriais;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaControleMateriaisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaControleMateriaisApplication.class, args);
	}

}
